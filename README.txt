/* CS 2 Homework 9 */
/* Eric Wang, Sei Masuoka */
/* README.txt */

We mainly worked together and wrote the code entirely together. Eric
wrote the initial code for creating the very simple heuristic game 
and a non working Alpha-Beta Pruning and Sei worked on improving each 
version to make them competitive, such as adding further heuristics
to focus on different spaces having different weightings. 
Mainly we worked together and discussed ideas until we agreed that
our strategy could be implemented and was a good idea, before taking
writing/debugging the code.

We improved our AI to make it tournament worthy by making it faster
to ensure we would never time out. Also, our current best implementation
has depth of only 2, which easily will have no problem in exceeding 
the time limit of the game.

