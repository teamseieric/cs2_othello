/* CS 2 Homework 9 */
/* Eric Wang, Sei Masuoka */
/* exampleplayer.cpp */
/**/

#include "exampleplayer.h"

/*
 * Constructor for the player; initialize everything here. The side your AI is
 * on (BLACK or WHITE) is passed in as "side". The constructor must finish 
 * within 30 seconds.
 */
ExamplePlayer::ExamplePlayer(Side side) {

    /* initialize a board for the player */
    board = new Board();
    /* keep track of our side */
    myside = side;
    /* set how deep you want your alpha-beta pruning to be */
    depth = 2;
}

/*
 * Destructor for the player.
 */
ExamplePlayer::~ExamplePlayer() {
}


/* alpha beta pruning function for othello game */

int AlphaBetaPruning(Move* move, int depth, int alpha, int beta, Side side, int player, Board* board, Side myside) {
    
    /* calculate opposite side coloring */
    Side oppositeside; 
    if (side == WHITE) {
        oppositeside = BLACK;
    }
    else if (side == BLACK) {
        oppositeside = WHITE;
    }
    /* make the opponents move on the new board */
    Board* copyboard = board->copy();
    copyboard->doMove(move, oppositeside);

    /* check if at max depth or nothing to do on board anymore */
    if ((depth == 0) || (copyboard->isDone())) { 
        int temp = 0;
        /* calculate heuristic based on actual player colors */
	    if(myside == WHITE) {
            temp = copyboard->count(myside) - copyboard->count(BLACK);
            oppositeside = BLACK;
        }
        else {
            temp = copyboard->count(myside) - copyboard->count(WHITE);
            oppositeside = WHITE;
        }
        /* affect the score with further weighting on corners */
        if(copyboard->get(myside, 0, 7))
            temp += 5;
        if(copyboard->get(myside, 7, 7))
            temp += 5;
        if(copyboard->get(myside, 0, 0))
            temp += 5;
        if(copyboard->get(myside, 7, 0))
            temp += 5;
        if(copyboard->get(oppositeside, 0, 7))
            temp -= 5;
        if(copyboard->get(oppositeside, 7, 7))
            temp -= 5;
        if(copyboard->get(oppositeside, 0, 0))
            temp -= 5;
        if(copyboard->get(oppositeside, 7, 0))
            temp -= 5;
        /* also take into account edge spaces weighting */
	    int i = 0;
	    int j = 7;
	    j = 0;
	    for (i=1;i<7;i++) {
	        if (copyboard->get(myside,j,i))
		        temp += 3;
	        if (copyboard->get(myside,i,j))
		        temp += 3;
	        if (copyboard->get(oppositeside,j,i))
		        temp -= 3;
	        if (copyboard->get(oppositeside,i,j))
		    temp -= 3;
	    }
	    j = 7;
	    for (i=1;i<7;i++) {
	        if (copyboard->get(myside,j,i))
		        temp += 3;
	        if (copyboard->get(myside,i,j))
		        temp += 3;
	        if (copyboard->get(oppositeside,j,i))
		        temp -= 3;
	        if (copyboard->get(oppositeside,i,j))
		        temp -= 3;
	    }
        
        return temp;
    }
 
    /* move variable to iterate through possible moves */
    Move* mymove = new Move(0,0);
    // maximization player
    if (player == MAXPLAYER) { 
        /* play on if you have no moves */
        if(!(copyboard->hasMoves(side))) {
            alpha = max(alpha, AlphaBetaPruning(NULL, depth-1, alpha, beta, oppositeside, player*(-1), copyboard, myside));
        }
        else {
            for (int x = 0; x < 8; x++) {
                mymove->setX(x);
                for (int y = 0; y < 8; y++) {
                    mymove->setY(y);
                    if (copyboard->checkMove(mymove, side)) {
                        alpha = max(alpha, AlphaBetaPruning(mymove, depth-1, alpha, beta, oppositeside, player*(-1), copyboard, myside));
                        /* alpha cut off */ 
                        if (beta <= alpha) {
                            break;
                        }
                    }
                }
            }
        }
        delete copyboard;
        delete mymove;
        return alpha;
    }
    // minimization player
    else { 
        if(!(copyboard->hasMoves(side))) {
            beta = min(beta, AlphaBetaPruning(NULL, depth-1, alpha, beta, oppositeside, player*(-1), copyboard, myside));
        }
        else {
            for (int x = 0; x < 8; x++) {
                mymove->setX(x);
                for (int y = 0; y < 8; y++) {
                    mymove->setY(y);
                    if (copyboard->checkMove(mymove, side)) {
                        beta = min(beta, AlphaBetaPruning(mymove, depth-1, alpha, beta, oppositeside, player*(-1), copyboard, myside));
                        /* beta cut off */
                        if (beta <= alpha) {
                            break;
                        }
                    }
                }
            }
        }
        delete copyboard;
        delete mymove;
        return beta;
    }
}


/*
 * Compute the next move given the opponent's last move. Each AI is
 * expected to keep track of the board on its own. If this is the first move,
 * or if the opponent passed on the last move, then opponentsMove will be NULL.
 *
 * If there are no valid moves for your side, doMove must return NULL.
 *
 * Important: doMove must take no longer than the timeout passed in 
 * msLeft, or your AI will lose! The move returned must also be legal.
 */


Move *ExamplePlayer::doMove(Move *opponentsMove, int msLeft) {

    /* make opponents on our board first */
    Side oppositeside; 
    if (myside == WHITE) {
        oppositeside = BLACK;
    }
    else if (myside == BLACK) {
        oppositeside = WHITE;
    }
    board->doMove(opponentsMove, oppositeside);

    Move* mymove = new Move(0,0);
    Move* bestmove = new Move(0,0);
    Board* copyboard = board->copy();
    int player = MAXPLAYER; // we want to maximize
    
    int bestvalue;
    /* start alpha at very low number, beta at very high number */
    int alpha = -10000;
    int beta = 10000;
    /* check if a move is possible */
    if (board->hasMoves(myside)) {
        for (int x = 0; x < 8; x++) {
            mymove->setX(x);
            for (int y = 0; y < 8; y++) {
                mymove->setY(y);
                if (copyboard->checkMove(mymove, myside)) {
                    bestvalue =  AlphaBetaPruning(mymove, depth-1, alpha, beta, oppositeside, player*(-1), copyboard, myside);
                    /* calculate maximum */
                    if (bestvalue >= alpha) {
                        alpha = bestvalue;
                        /* keep track of your best move */
                        bestmove = mymove->moveCopy();
                    }
                }
            }
        }
        delete mymove;
        delete copyboard;
        /* make the move on your board */
        board->doMove(bestmove,myside);
        return bestmove;
    }

    delete mymove;
    delete copyboard;
    /* return NULL if you can't do anything */
    return NULL;
}
    
