/* CS 2 Homework 9 */
/* Eric Wang, Sei Masuoka */
/* exampleplayer.h */

#ifndef __EXAMPLEPLAYER_H__
#define __EXAMPLEPLAYER_H__

#include <iostream>
#include "common.h"
#include "board.h"
#include <algorithm> 

/* constants for alpha-beta pruning */
#define MAXPLAYER 1
#define MINPLAYER -1

using namespace std;

class ExamplePlayer {

private:
    Board* board;
    Side myside;
    int depth;

public:
    ExamplePlayer(Side side);
    ~ExamplePlayer();
    
    Move *doMove(Move *opponentsMove, int msLeft);
};

#endif
